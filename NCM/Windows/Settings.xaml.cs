﻿using NCM.Classes;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace NCM.Windows
{
    /// <summary>
    /// Interaktionslogik für Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        private EnviromentBuilder envBuilder = null;
        public Settings()
        {
            InitializeComponent();
            envBuilder = new EnviromentBuilder();
            tbHeader.Text += Environment.NewLine;
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Owner.IsEnabled = true;
            Hide();
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        public void Show(Window owner)
        {
            Owner = owner;
            Show();
        }

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in stackSettings.Children)
            {
                if (item.GetType() == typeof(CheckBox))
                {
                    if (((CheckBox)item).IsChecked == true)
                    {
                        if (((CheckBox)item).Name == cbDeleteFiles.Name)
                        {
                            envBuilder.FolderChecker();
                        }
                        ((CheckBox)item).IsChecked = false;
                    }
                }
            }
            Owner.IsEnabled = true;
            Hide();
        }
    }
}
