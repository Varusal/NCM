﻿using NCM.Classes;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace NCM.Windows
{
    /// <summary>
    /// Interaktionslogik für Home.xaml
    /// </summary>
    public partial class Home : Window
    {
        private EnviromentBuilder envBuilder = null;
        private Settings setWin = null;
        private string resource = null;
        private NetworkControllerLoginHandler ncLoginHandler = null;
        private Details detail = null;
        public Home(NetworkControllerLoginHandler ncLoginHandler)
        {
            InitializeComponent();
            setWin = new Settings();
            resource = @"/";
            this.ncLoginHandler = ncLoginHandler;
            envBuilder = new EnviromentBuilder();
        }

        private void imSettings_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            IsEnabled = false;
            setWin.Show(this);
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        public void Show(Window owner)
        {
            Owner = owner;
            Show();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void tbxNCInfo_Click(object sender, RoutedEventArgs e)
        {
            tbxNCInfo.Foreground = Brushes.Black;
            tbxNCInfo.FontSize = 14;
            //tbxVmmInfo.Foreground = Brushes.LightGray;
            //tbxVmmInfo.FontSize = 12;
            dockNCInfo.Visibility = Visibility.Visible;
        }

        private void tbxVmmInfo_Click(object sender, RoutedEventArgs e)
        {
            //tbxVmmInfo.Foreground = Brushes.Black;
            //tbxVmmInfo.FontSize = 14;
            tbxNCInfo.Foreground = Brushes.LightGray;
            tbxNCInfo.FontSize = 12;
            dockNCInfo.Visibility = Visibility.Hidden;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (resource.Length > 1)
            {
                resource = @"/";
            }
            resource += ((Button)sender).Content.ToString();
            resource = resource.Replace(" ", string.Empty);
            resource = resource.ToLower();
            if (resource != @"/virtualnetworkmanager" && resource != @"/idnsserver" && resource != @"/virtualswitchmanager")
            {
                ncLoginHandler.GetSelected(resource);
            }
            else if (resource == @"/virtualnetworkmanager" || resource == @"/idnsserver" || resource == @"/virtualswitchmanager")
            {
                resource += @"/configuration";
                ncLoginHandler.GetSelected(resource);
            }
            if (File.Exists(envBuilder.filePath + @"\" + ncLoginHandler.fileName))
            {
                detail = new Details(ncLoginHandler.fileName, ncLoginHandler);
                detail.ShowDialog(this);
            }
        }
    }
}
