﻿using System;
using System.Windows;
using System.Windows.Input;
using NCM.Classes;
using System.Threading;
using System.Windows.Threading;
using System.Security;
using System.Windows.Media;
using System.Windows.Controls;

namespace NCM.Windows
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class Login : Window
    {
        Home h = null;
        NetworkControllerLoginHandler ncLoginHandler = null;
        EnviromentBuilder envBuilder = null;
        private ThreadStart loginThreadStart = null;
        private Thread loginThread = null;
        private SecureString password = new SecureString();
        private string user = null;
        private string ncUrl = null;
        private bool login;
        public Login()
        {
            InitializeComponent();
            login = false;
            envBuilder = new EnviromentBuilder();
            ncLoginHandler = new NetworkControllerLoginHandler();
            h = new Home(ncLoginHandler);
            tbxNCUrl.Text = Properties.Settings.Default.BaseUrl;
            tbxUser.Text = Properties.Settings.Default.BaseUser;
            envBuilder.FolderChecker();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            if (tbxNCUrl.Text != "" && tbxUser.Text != "" && pbxPassword.Password != "")
            {
                IsEnabled = false;
                Properties.Settings.Default.BaseUrl = tbxNCUrl.Text;
                Properties.Settings.Default.BaseUser = tbxUser.Text;
                Properties.Settings.Default.Save();
                pgLogin.Visibility = Visibility.Visible;
                loginThreadStart = new ThreadStart(loginThread_DoWork);
                loginThread = new Thread(loginThreadStart);
                loginThread.Start();
            }
            else
            {
                foreach (var item in ncURLStack.Children)
                {
                    if (item.GetType() == typeof(TextBox))
                    {
                        if (((TextBox)item).Text == "")
                        {
                            ((TextBox)item).BorderBrush = Brushes.Red;
                        }
                    }
                }

                foreach (var item in userStack.Children)
                {
                    if (item.GetType() == typeof(TextBox))
                    {
                        if (((TextBox)item).Text == "")
                        {
                            ((TextBox)item).BorderBrush = Brushes.Red;
                        }
                    }
                }

                foreach (var item in passwordStack.Children)
                {
                    if (item.GetType() == typeof(PasswordBox))
                    {
                        if (((PasswordBox)item).Password == "")
                        {
                            ((PasswordBox)item).BorderBrush = Brushes.Red;
                        }
                    }
                }
            }
        }

        private void loginThread_DoWork()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Action)(() =>
            {
                password.Clear();
                foreach (char item in pbxPassword.Password)
                {
                    password.AppendChar(item);
                }
                user = tbxUser.Text;
                ncUrl = tbxNCUrl.Text;
                login = ncLoginHandler.NetworkControllerLogin(user, password, ncUrl, "/internalResourceInstances");
                pbxPassword.Clear();
                switch (login)
                {
                    case true:
                        pgLogin.Visibility = Visibility.Hidden;
                        h.Show(this);
                        IsEnabled = true;
                        Close();
                        break;
                    case false:
                        pgLogin.Visibility = Visibility.Hidden;
                        IsEnabled = true;
                        break;
                }
            }));
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    btnConnect_Click(sender, e);
                    break;
            }
        }
    }
}
