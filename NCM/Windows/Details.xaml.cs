﻿using NCM.Classes;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace NCM.Windows
{
    /// <summary>
    /// Interaktionslogik für Details.xaml
    /// </summary>
    public partial class Details : Window
    {
        private string fileName = null;
        private EnviromentBuilder envBuilder = null;
        private JSONReader jsonReader = null;
        private NetworkControllerLoginHandler ncLoginHandler = null;
        public Details(string fileName, NetworkControllerLoginHandler ncLoginHandler)
        {
            InitializeComponent();
            this.fileName = fileName;
            envBuilder = new EnviromentBuilder();
            jsonReader = new JSONReader(this);
            this.ncLoginHandler = ncLoginHandler;
            ShowContent();
        }

        private void ShowContent()
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Action)(() =>
            {
                jsonReader.GetResourceId(fileName);
            }));
        }

        public void Button_Click(object sender, RoutedEventArgs e)
        {
            ncLoginHandler.GetSelected(((Button)sender).Content.ToString());
            fileName = ((Button)sender).Content.ToString();
            fileName = fileName.Remove(0, 1);
            fileName = fileName.Replace("/", "-");
            fileName += ".json";
            Dispatcher.BeginInvoke(DispatcherPriority.Normal, (Action)(() =>
            {
                jsonReader.GetContent(fileName);
            }));
            dockBtn.Visibility = Visibility.Hidden;
            dockInfo.Visibility = Visibility.Visible;
        }

        public void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        public void ShowDialog(Window owner)
        {
            Owner = owner;
            ShowDialog();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            switch (dockInfo.Visibility)
            {
                case Visibility.Visible:
                    dockInfo.Visibility = Visibility.Hidden;
                    dockBtn.Visibility = Visibility.Visible;
                    Hide();
                    break;
                case Visibility.Hidden:
                    Hide();
                    break;
            }
        }
    }
}
