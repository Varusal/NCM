﻿using NCM.Windows;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace NCM.Classes
{
    public class JSONReader
    {
        private string resource = null;
        private EnviromentBuilder envBuilder = null;
        public List<string> content = null;
        private Details details = null;
        private string btnName = null;
        private string text = null;
        private string tempFile = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\NCM\temp.txt";
        private Button btn = null;
        public JSONReader()
        {
            resource = default(string);
            envBuilder = new EnviromentBuilder();
            btnName = "btn";
            content = new List<string>();
            text = default(string);
        }

        public JSONReader(Details details)
        {
            resource = default(string);
            envBuilder = new EnviromentBuilder();
            this.details = details;
            btnName = "btn";
            content = new List<string>();
            text = default(string);
        }
        public void GetResourceId(string resource)
        {
            this.resource = envBuilder.filePath + @"\" + resource;
            if (File.Exists(this.resource))
            {
                if (File.ReadAllText(this.resource) != null && File.ReadAllText(this.resource) != "")
                {
                    dynamic obj = JObject.Parse(File.ReadAllText(this.resource));
                    try
                    {
                        for (int i = 0; i < obj.value.Count; i++)
                        {
                            CreateButtons(obj.value[i].resourceRef.ToString());
                        }
                    }
                    catch
                    {
                        try
                        {
                            if (obj.resourceRef != null)
                            {
                                if (obj.resourceRef.ToString() != "/virtualNetworkManager/")
                                {
                                    CreateButtons(obj.resourceRef.ToString());
                                }
                                else if (obj.resourceRef.ToString() == "/virtualNetworkManager/")
                                {
                                    CreateButtons(obj.resourceRef.ToString() + "configuration");
                                }
                            }
                            else if (resource.Contains("idnsserver"))                         
                            {
                                CreateButtons("/idnsserver/configuration");
                            }
                            else
                            {
                                CreateButtons("No entries found!");
                            }
                        }
                        catch
                        {
                            CreateButtons("No entries found!");
                        }
                    }
                }
                else
                {
                    CreateButtons("No entries found!");
                }
            }
        }

        private void CreateButtons(string name)
        {
            content.Add(name);
            btn = new Button();
            btn.Content = name;
            btnName += name;
            btnName = btnName.Replace("-", string.Empty);
            btnName = btnName.Replace("/", string.Empty);
            btnName = btnName.Replace(" ", string.Empty);
            if (btnName.Contains("_"))
            {
                btnName = btnName.Replace("_", string.Empty);
            }
            if (btnName.Contains("."))
            {
                btnName = btnName.Replace(".", string.Empty);
            }
            if (btnName.Contains("!"))
            {
                btnName = btnName.Replace("!", string.Empty);
            }
            btn.Name = btnName;
            if (btn.Name == "btnNoentriesfound")
            {
                btn.IsEnabled = false;
            }
            btn.Width = 400;
            btn.Style = (Style)Application.Current.Resources["ButtonCategories"];
            btn.Click += details.Button_Click;
            details.stackLeft.Children.Add(btn);
        }

        public void GetContent(string fileName)
        {
            resource = envBuilder.filePath + @"\" + fileName;
            if (File.Exists(resource))
            {
                if (File.ReadAllText(resource) != null && File.ReadAllText(resource) != "")
                {
                    text = File.ReadAllText(resource);
                    text = text.Replace("\"", string.Empty);
                    text = text.Replace("{", string.Empty);
                    text = text.Replace("}", string.Empty);
                    text = text.Replace("[", string.Empty);
                    text = text.Replace("]", string.Empty);
                    text = text.Replace(" ", string.Empty);
                    text = text.Replace(",", string.Empty);
                    var lines = text.Split('\n').Where(s => !string.IsNullOrWhiteSpace(s));
                    text = string.Join("\n", lines);
                    text = text.Replace(":", ": ");
                    File.WriteAllText(tempFile, text);
                    var test = File.ReadAllLines(tempFile);
                    foreach (string item in test)
                    {
                        var ding = item.IndexOf(":");
                        if (ding != item.Length - 2)
                        {
                            TextBox tbx = new TextBox();
                            tbx.Text = item;
                            tbx.IsReadOnly = true;
                            tbx.Style = (Style)Application.Current.Resources["TbxInfo"];
                            details.stackInfo.Children.Add(tbx);
                        }
                        else
                        {
                            TextBox tbx = new TextBox();
                            tbx.Text = string.Empty;
                            tbx.IsReadOnly = true;
                            tbx.Style = (Style)Application.Current.Resources["TbxInfo"];
                            details.stackInfo.Children.Add(tbx);

                            tbx = new TextBox();
                            tbx.Text = item;
                            tbx.Style = (Style)Application.Current.Resources["TbxInfo"];
                            tbx.FontWeight = FontWeights.Bold;
                            tbx.IsReadOnly = true;
                            details.stackInfo.Children.Add(tbx);
                        }
                    }
                }
            }
        }
    }
}
