﻿using System;
using System.IO;

namespace NCM.Classes
{
    public class EnviromentBuilder
    {
        private string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\NCM";
        public string filePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\NCM";
        public string content = null;
        private DirectoryInfo dirInfo = null;

        public EnviromentBuilder()
        {
            content = default(string);
        }
        public void FolderChecker()
        {
            switch (Directory.Exists(folderPath))
            {
                case false:
                    Directory.CreateDirectory(folderPath);
                    break;
                case true:
                    dirInfo = new DirectoryInfo(folderPath);
                    foreach (FileInfo file in dirInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in dirInfo.GetDirectories())
                    {
                        dir.Delete();
                    }
                    break;
            }
        }

        public void SetBasePath()
        {
            filePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\NCM";
        }
        public void SaveJson(string path, string content)
        {
            SetBasePath();
            filePath += @"\" + path;
            try
            {
                File.WriteAllText(filePath, content);
            }
            catch
            {
                
            }
        }

    }
}
