﻿using RestSharp;
using System.Net;
using System.Security;
using System.Text;

namespace NCM.Classes
{
    public class NetworkControllerLoginHandler
    {
        private string ncUrl = null;
        private string ncUrlUpdated = null;
        private string resource = null;
        private string user = null;
        private SecureString password = null;
        private bool login;
        private bool domain;
        private RestClient client = null;
        private RestRequest request = null;
        public string fileName = null;
        private EnviromentBuilder envBuilder = null;
        
        public NetworkControllerLoginHandler()
        {
            envBuilder = new EnviromentBuilder();
            ncUrl = "https://{0}/networking/v1";
            resource = default(string);
            user = default(string);
            password = new SecureString();
            login = false;
            fileName = default(string);
            domain = false;
        }

        public bool NetworkControllerLogin(string user, SecureString password, string nc, string resource)
        {
            try
            {
                ncUrlUpdated = ncUrl.Replace("{0}", nc);
                this.resource = resource;
                this.user = user;
                this.password = password;
                GetSelected(this.resource);
                return login;
            }
            catch
            {
                return login;
            }
        }

        public void GetSelected(string resource)
        {
            if (resource == @"/loadbalancermanager")
            {
                this.resource = @"/loadbalancermanager/config";
            }
            else
            {
                this.resource = resource;
            }
            login = true;
            client = new RestClient(ncUrlUpdated);
            request = new RestRequest(this.resource, Method.GET);
            fileName = this.resource.Remove(0, 1);

            if (fileName.Contains("/"))
            {
                fileName = fileName.Replace("/", "-");
            }
            fileName += ".json";

            if (user.Contains("\\"))
            {
                domain = true;
            }

            switch (domain)
            {
                case true:
                    string[] userCred = user.Split('\\');
                    request.Credentials = new NetworkCredential(userCred[1], password, userCred[0]);
                    break;
                case false:
                    request.Credentials = new NetworkCredential(user, password);
                    break;
            }
            byte[] response = client.DownloadData(request);
             if (response != null)
            {
                string str = Encoding.Default.GetString(response);
                switch (str)
                {
                    case "":
                        login = false;
                        break;
                }
                envBuilder.SaveJson(fileName, str);
            }
            else
            {
                login = false;
            }
        }
    }
}
